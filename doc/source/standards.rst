Standards support
=================

Here are some important parts of the RFCs that define HTTP that refers to proxy.

Compliance with standards is a major goal of ``panosh``, and support for
everything listed below will be added.


RFC 7230
--------

:name: Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing

3.2.1.  Field Extensibility
"""""""""""""""""""""""""""

A proxy MUST forward unrecognized header fields unless the field-name is
listed in the Connection header field (Section 6.1) or the proxy is
specifically configured to block, or otherwise transform, such fields.


5.7.  Message Forwarding
""""""""""""""""""""""""

An intermediary not acting as a tunnel MUST implement the Connection
header field, as specified in Section 6.1, and exclude fields from being
forwarded that are only intended for the incoming connection.


5.7.1.  Via
"""""""""""

A proxy MUST send an appropriate Via header field, as described below,
in each message that it forwards.
For example, a request message could
be sent from an HTTP/1.0 user agent to an internal proxy code-named
"fred", which uses HTTP/1.1 to forward the request to a public proxy at
p.example.net, which completes the request by forwarding it to the
origin server at www.example.com.  The request received by
www.example.com would then have the following Via header field:

  Via: 1.0 fred, 1.1 p.example.net


5.7.2.  Transformations
"""""""""""""""""""""""

A proxy MAY modify the message body through application or removal of a
transfer coding (Section 4).

A proxy MUST NOT transform the payload (Section 3.3 of [RFC7231]) of a
message that contains a no-transform cache-control directive (Section
5.2 of [RFC7234]).

A proxy MAY transform the payload of a message that does not contain a
no-transform cache-control directive.  A proxy that transforms a payload
MUST add a Warning header field with the warn-code of 214
("Transformation Applied") if one is not already in the message (see
Section 5.5 of [RFC7234]).  A proxy that transforms the payload of a 200
(OK) response can further inform downstream recipients that a
transformation has been applied by changing the response status code to
203 (Non-Authoritative Information) (Section 6.3.4 of [RFC7231]).

A proxy SHOULD NOT modify header fields that provide information about
the endpoints of the communication chain, the resource state, or the
selected representation (other than the payload) unless the field's
definition specifically allows such modification or the modification is
deemed necessary for privacy or security.



6.1.  Connection
""""""""""""""""

The "Connection" header field allows the sender to indicate desired
control options for the current connection.  In order to avoid confusing
downstream recipients, a proxy or gateway MUST remove or replace any
received connection options before forwarding the message.

When a header field aside from Connection is used to supply
controlinformation for or about the current connection, the sender MUST
list the corresponding field-name within the Connection header field.  A
proxy or gateway MUST parse a received Connection header field before a
message is forwarded and, for each connection-option in this field,
remove any header field(s) from the message with the same name as the
connection-option, and then remove the Connection header field itself
(or replace it with the intermediary's own connection options for the
forwarded message).


6.3.  Persistence
"""""""""""""""""

A proxy server MUST NOT maintain a persistent connection with an
HTTP/1.0 client (see Section 19.7.1 of [RFC2068] for information and
discussion of the problems with the Keep-Alive header field implemented
by many HTTP/1.0 clients).

