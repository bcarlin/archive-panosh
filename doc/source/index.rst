.. panosh documentation master file, created by
   sphinx-quickstart on Mon Jun 29 18:20:55 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to panosh's documentation!
==================================

Panosh aims to be a privacy-enhancing, anonymity preserving,
lightweight personal HTTP proxy.

This is an early development and certainly not ready for anything yet.

Planned Features are :

- Force HTTPS if available
- Support HTTP
- Support HTTPS
- Do not block browser cache
- Do not block Websockets
- Implement a cache at the proxy level
- Keep connections alive
- Filter out HTTP requests for ads
- Block privacy-invading cookies
- Sanitize Pages content
- Option to leave no log
- Do not track headers injection

Contents:

.. toctree::
   :maxdepth: 2

   getting-started
   configuration
   standards




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

