Configuration
=============

``panosh`` can be configured on the command line or with environment variables.

For now, following options are supported:

================ =============== ======= ============================================
CLI argument     Env var         Default Signification
================ =============== ======= ============================================
``-p, --port=``  ``PANOSH_PORT`` 8080    The port the proxy listens to
================ =============== ======= ============================================
