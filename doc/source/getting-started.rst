Getting Started
===============


1. Get the sources. You can fetch the sources from Bitbucket::

     git clone https://bitbucket.org/bcarlin/panosh.git

2. Build the sources. To build panosh youshould use gb_::

     cd panosh
     gb build

   If you don't have gb installed, `follow these instructions <Install gb_>`_.

3. Run panosh with the command ::

      ./bin/panosh

.. _gb: http://getgb.io
.. _Install gb: http://getgb.io/docs/install/