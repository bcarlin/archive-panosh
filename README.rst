Panosh
======

Panosh aims to be a privacy-enhancing, anonymity preserving,
lightweight personal HTTP proxy.

This is an early development and certainly not ready for anything yet.


Installation
============

To fetch this project::

  git clone https://bitbucket.org/bcarlin/panosh.git

Then, build it with gb_::

  cd panosh
  gb build

If you don't have gb installed, `follow these instructions <Install gb_>`_.

.. _gb: http://getgb.io
.. _Install gb: http://getgb.io/docs/install/


Usage
-----

To start the proxy, build it, and then run the binary ``./bin/panosh``::

   $ ./bin/panosh -h
   Usage:
     panosh [OPTIONS]

   Application Options:
     -p, --port= The port the proxy listens to (8080) [$PANOSH_PORT]

   Help Options:
     -h, --help  Show this help message



Contributing
------------

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -m 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D


License
-------

Panosh is licensed under the terms of the MIT license.