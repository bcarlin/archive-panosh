package main

import (
	"fmt"
	"os"

	"github.com/jessevdk/go-flags"
)

func main() {
	var conf Config
	_, err := flags.Parse(&conf)
	if err != nil {
		os.Exit(2)
	}
	if err := Start(conf); err != nil {
		fmt.Printf("Error: %s\n", err.Error())
		os.Exit(3)
	}
}
