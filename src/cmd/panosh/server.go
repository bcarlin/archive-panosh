package main

import (
	"fmt"
	"net/http"
)

type Config struct {
	Port int `short:"p" long:"port" default:"8080" description:"The port the proxy listens to" env:"PANOSH_PORT"`
}

func Start(conf Config) error {
	proxy := NewProxy()
	return http.ListenAndServe(fmt.Sprintf("127.0.0.1:%d", conf.Port),
		proxy)
}
