package main

import (
	"net/http"
	"testing"
)

func TestRequestDropConnectionHeaders(t *testing.T) {
	h1 := http.Header{
		"Foo":        []string{"blah"},
		"Bar":        []string{"blih"},
		"Baz":        []string{"bluh"},
		"Connection": []string{"Foo, baz"},
	}

	h2 := http.Header{}

	copyHeaders(h2, h1)

	t.Logf("h2: %#v", h2)

	if _, ok := h2["Foo"]; ok {
		t.Error("Foo header is found but it shouldn't: it is in Connection")
	}

	if _, ok := h2["Baz"]; ok {
		t.Error("Baz header is found but it shouldn't: it is in Connection")
	}

	if _, ok := h2["Connection"]; ok {
		t.Error("Connection header is found but it shouldn't: it must be removed")
	}

	if _, ok := h2["Bar"]; ok {
		if h2.Get("Bar") != "blih" {
			t.Error("The value of the Bar header has been modified")
		}
	} else {
		t.Error("Baz header is found but it shouldn't: it must be removed")
	}
}
