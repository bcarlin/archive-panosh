package main

import (
	"io"
	"log"
	"net/http"
	"strings"
)

type Proxy struct {
	client http.Client
}

func NewProxy() *Proxy {
	return &Proxy{
		client: http.Client{},
	}
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	defer func() {
		//FIXME: export that in a middleware
		var (
			res    = "OK"
			errMsg = "-"
		)
		if err != nil {
			res = "KO"
			errMsg = err.Error()
		}
		log.Printf("[%s] %s %s\n", res, r.URL, errMsg)
	}()

	req, err := http.NewRequest(r.Method, r.RequestURI, r.Body)
	if err != nil {
		return
	}

	copyHeaders(req.Header, r.Header)

	resp, err := p.client.Do(req)
	if err != nil {
		return
	}

	wHeaders := w.Header()
	wHeaders.Set("X-Proxy", "panosh")
	copyHeaders(wHeaders, resp.Header)
	w.WriteHeader(resp.StatusCode)

	_, err = io.Copy(w, resp.Body)
	resp.Body.Close()
	if err != nil {
		return
	}
}

func copyHeaders(dst http.Header, src http.Header) {
	skip := strings.ToLower(src.Get("Connection"))

	for name, value := range src {
		if !strings.Contains(skip, strings.ToLower(name)) && name != "Connection" {
			dst[name] = value
		}
	}
}
